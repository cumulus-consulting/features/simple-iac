# Simple IAC

This repo stores playbooks that can be used to create a simple IAC environment where configurations are backed up and restored.

Supported Operating systems and configuration mechanisms include:

```
Cumulus
  flat files
  NVUE

SONIC
  config_db.json
```